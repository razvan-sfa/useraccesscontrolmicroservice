package com.setmobile.uacms.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.setmobile.uacms.entities.MobileDeviceEntity;
import com.setmobile.uacms.entities.TenantEntity;
import com.setmobile.uacms.model.AffiliateDeviceModel;
import com.setmobile.uacms.model.AffiliateRequestModel;
import com.setmobile.uacms.model.ExportTenantsModel;
import com.setmobile.uacms.model.MobileDeviceModel;
import com.setmobile.uacms.model.TenantModel;
import com.setmobile.uacms.repositories.InstanceRepository;
import com.setmobile.uacms.repositories.MobileDeviceRepository;
import com.setmobile.uacms.repositories.TenantRepository;
import com.setmobile.uacms.util.BackofficeConnection;
import com.setmobile.uacms.util.DateUtil;
import com.setmobile.uacms.util.InstanceLookupService;
import com.setmobile.uacms.util.TenantUtil;

import sun.misc.BASE64Encoder;

@Component
public class UserAccessControlWebServices {

	private static Logger log = Logger.getLogger(UserAccessControlWebServices.class);
	
	@Value("${amazon.webservices.url}")
	String AMAZON_WEBSERVICES_INSTANCE_GENERATOR_URL;

	@Value("${amazon.instances.threshold}")
	String multitenaceToInstanceThreshold;

	@Value("${frontoffice.url}")
	String frontOfficeUrl;

	MobileDeviceRepository mobileDeviceRepository;
	TenantRepository tenantRepository;
	InstanceRepository instanceRepository;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private InstanceLookupService instanceLookupService;
	
	@Autowired
	private BackofficeConnection backofficeConnection;
	
	@Autowired
	private TenantUtil tenantUtil;
	
	@Autowired
	public UserAccessControlWebServices(MobileDeviceRepository mobileDeviceRepository,
			TenantRepository tenantRepository, InstanceRepository instanceRepository) {
		super();
		this.mobileDeviceRepository = mobileDeviceRepository;
		this.tenantRepository = tenantRepository;
		this.instanceRepository = instanceRepository;
	}
	
	@GET
	@Path("/getSomething")
	public void getSomething(){
		TenantEntity tenantEntity = new TenantEntity();
		tenantEntity = tenantRepository.findOne(80);
		MobileDeviceEntity mobileDeviceEntity = new MobileDeviceEntity();
		mobileDeviceEntity = mobileDeviceRepository.findOne(19);
		//backofficeConnection.addTenantFolderStructure(tenantEntity);
		backofficeConnection.addAgent(tenantEntity, "john-user", "john-pass", "john-nume", "john@email", "2385732","359283051348711");
	}
	
	@POST
	@Path("/affiliateRequest")
	@Consumes("application/json")
	@Produces("application/json")
	public Response affiliateRequest(AffiliateRequestModel affiliateRequestModel){
		log.info("AffiliateRequest " + affiliateRequestModel.toString());
		TenantEntity tenantEntity = tenantRepository.findByEmail(affiliateRequestModel.getEmail());
		if (tenantEntity != null){
			log.info("This tenant exists in the database.");
			MobileDeviceEntity mobileDeviceEntity = mobileDeviceRepository.findByImei(affiliateRequestModel.getImei());
			if (mobileDeviceEntity == null){
				mobileDeviceEntity = new MobileDeviceEntity();
				log.info("Insert mobile device in the database.");
				mobileDeviceEntity.setImei(affiliateRequestModel.getImei());
				mobileDeviceEntity.setImsi(affiliateRequestModel.getImsi());
				mobileDeviceEntity.setUser(affiliateRequestModel.getName());
				mobileDeviceEntity.setPass(affiliateRequestModel.getPassword());
				mobileDeviceEntity = mobileDeviceRepository.save(mobileDeviceEntity);
			} else {
				log.info("Mobile device already exists in the database.");
				mobileDeviceEntity.setUser(affiliateRequestModel.getName());
				mobileDeviceEntity.setPass(affiliateRequestModel.getPassword());
				mobileDeviceEntity = mobileDeviceRepository.save(mobileDeviceEntity);
				
			}
			if (mobileDeviceEntity.getTenant() != null){
				log.info("Mobile device is affiliated to tenant with id:" + mobileDeviceEntity.getTenant().getTntId());
			} else {
				log.info("Mobile device is not affiliated. Sending request to tenant with id:" + tenantEntity.getTntId());
				MimeMessage message = javaMailSender.createMimeMessage();
				MimeMessageHelper helper;
				try {
					helper = new MimeMessageHelper(message, true);
					helper.setSubject("Request for affiliation");
					helper.setTo(tenantEntity.getEmail());
					String name = affiliateRequestModel.getName()!=null?affiliateRequestModel.getName():"Someone";
					String body = "<html> <body>" + "<h2>" + name + " with phone IMEI " + affiliateRequestModel.getImei() + " wants to join your team!</h2>"
							+ "<p>To activate his subscription click the link below:</p> <a href='http://52.29.158.26:8081/services/uac/affiliateConfirmation?id=insertedId'>www.avansales.com/signUp?id=insertedId</a>"
							+ "<br> <br> Kind regards!<br> Avansales Cloud Team<br>" + "</body> </html>";
					ObjectMapper mapper = new ObjectMapper();
					String obfuscatedLink = null;
					try {
						obfuscatedLink = Base64.getEncoder().encodeToString(mapper.writeValueAsString(affiliateRequestModel).getBytes());
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					body = body.replaceAll("insertedId", obfuscatedLink);
					helper.setText(body, true);
					log.info("Sending email..." + body );
					javaMailSender.send(message);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}
		} else {
			log.info("This tenant does not exist in the database.");
		}
		return Response.ok().build();
	}
	
	@GET
	@Path("/affiliateConfirmation")
	public Response affiliateConfirmation(@QueryParam("id") String obfuscatedLink) {
		log.info("affiliateConfirmation");
		ObjectMapper mapper = new ObjectMapper();
		try {
			AffiliateRequestModel affiliateRequestModel = mapper.readValue(new String(Base64.getDecoder().decode(obfuscatedLink)), AffiliateRequestModel.class);
			log.info(affiliateRequestModel);
			MobileDeviceEntity mobileDeviceEntity = mobileDeviceRepository.findByImei(affiliateRequestModel.getImei());
			TenantEntity tenantEntity = tenantRepository.findByEmail(affiliateRequestModel.getEmail());
			mobileDeviceEntity.setTenant(tenantEntity);
			mobileDeviceRepository.save(mobileDeviceEntity);
			backofficeConnection.addAgent(tenantEntity, affiliateRequestModel.getName(), affiliateRequestModel.getPassword(), null, null, null, mobileDeviceEntity.getImei());
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Response.ok().build();
	}
	//WS NOT USED
	@POST
	@Path("/affiliateDevice")
	@Consumes("application/json")
	@Produces("application/json")
	public void affiliateDevice(AffiliateDeviceModel affiliateDeviceModel){
		log.info("affiliateDevice with imei: " + affiliateDeviceModel.getImei() + " to tenant id: " + affiliateDeviceModel.getTenant());
		TenantEntity tenantEntity = tenantRepository.findOne(affiliateDeviceModel.getTenant());
		//check is this mobile is in ACM database
		MobileDeviceEntity mobileDeviceEntity = mobileDeviceRepository.findByImei(affiliateDeviceModel.getImei());
		if (mobileDeviceEntity != null){
			log.info("Mobile device is already in the database");
			mobileDeviceEntity.setTenant(tenantEntity);
			mobileDeviceEntity = mobileDeviceRepository.save(mobileDeviceEntity);
			backofficeConnection.addDevice(tenantEntity, mobileDeviceEntity);			
		} else {
			log.info("Inserting mobile device in database");
			mobileDeviceEntity = new MobileDeviceEntity();
			mobileDeviceEntity.setImei(affiliateDeviceModel.getImei());
			mobileDeviceEntity.setImsi(affiliateDeviceModel.getImsi());
			mobileDeviceEntity.setTenant(tenantEntity);
			mobileDeviceEntity = mobileDeviceRepository.save(mobileDeviceEntity);
			backofficeConnection.addDevice(tenantEntity, mobileDeviceEntity);
		}
	}
	
	
	@POST
	@Path("/initializeTenant")
	@Consumes("application/x-www-form-urlencoded")
	public void initializeTenant(@FormParam("idTenant") Integer idTenant) {
		log.info("InitializeTenant with idTenant: " + idTenant.toString());
		backofficeConnection.initializeTenant(idTenant);
	}
	
	@GET
	@Path("/signUpConfirmation")
	public Response signUpConfirmation(@QueryParam("token") String verificationToken) {
		log.info("signUpConfirmation");
		log.info("verificationToken " + verificationToken);
		TenantEntity tenantEntity = tenantRepository.findByVerificationToken(verificationToken);
		log.info("Set tenant state demo for tenant: " + tenantEntity);
		tenantEntity.setTenantState("demo");
		tenantEntity.setDatain(DateUtil.getCurrentTimestamp());
		tenantRepository.save(tenantEntity);
		tenantEntity.setCIN(tenantUtil.getNextCustomerIdentificationNumber(tenantEntity));
		tenantRepository.save(tenantEntity);
		try {
			instanceLookupService.associateTenantToInstace(tenantEntity);
		} catch (InterruptedException e){
			e.printStackTrace();
		}
		
		
		try {
			return Response.temporaryRedirect(new URI(frontOfficeUrl + "/subscription/signIn")).build();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}

	@POST
	@Path("/sendMailConfirmation")
	@Consumes("application/x-www-form-urlencoded")
	public void sendMailConfirmation(@FormParam("idTenant") Integer idTenant) {
		log.info("sendMailConfirmation");
		log.info("IdTenant: " + idTenant.toString());
		TenantEntity tenantEntity = tenantRepository.findOne(idTenant);
		log.info("Tenant Email: " + tenantEntity.getEmail());
		String token = UUID.randomUUID().toString();
		tenantEntity.setVerificationToken(token);
		tenantRepository.save(tenantEntity);
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setSubject("Avansales Cloud account confirmation");
			helper.setTo(tenantEntity.getEmail());
			String body = "<html> <body>" + "<h2>Congratulations for Sing Up!</h2>"
					+ "<p>To start your own Avansales Cloud click the link below:</p> <a href='http://52.29.158.26:8081/services/uac/signUpConfirmation?token=insertedToken'>www.avansales.com/signUp?token=insertedToken</a>"
					+ "<br> <br> Kind regards!<br> Avansales Cloud Team<br>" + "</body> </html>";
			helper.setText(body.replaceAll("insertedToken", token), true);
			log.info("Sending email...");
			javaMailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
/*
	@POST
	@Path("/signup")
	@Consumes("application/json")
	public void signUp() {
		System.out.println("Enter SignUp");
		Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
		WebTarget target = client.target(AMAZON_WEBSERVICES_INSTANCE_GENERATOR_URL).path("/create");
		AwsEc2Instance instance = target.request().get(AwsEc2Instance.class);
		System.out.println(instance.getImage());
	}
*/
	@POST
	@Path("/signIn")
	@Consumes("application/json")
	@Produces("application/json")
	public TenantModel signIn(MobileDeviceModel mobileDeviceModel) {
		log.info(mobileDeviceModel);
		TenantModel tenantModel = new TenantModel();
		if (mobileDeviceModel.getImei() != null) {
			MobileDeviceEntity deviceFromRepository = mobileDeviceRepository.findByImei(mobileDeviceModel.getImei());
			log.info(deviceFromRepository);
			if (deviceFromRepository != null) {
				TenantEntity tenantEntity = deviceFromRepository.getTenant();
				log.info(tenantEntity);
				if (tenantEntity != null) {
					tenantModel.setBackofficeIp(tenantEntity.getBackofficeIp());
					tenantModel.setBackofficePort(tenantEntity.getBackofficePort());
					tenantModel.setSyncServerIp(tenantEntity.getSyncServerIp());
					tenantModel.setSyncServerPort(tenantEntity.getSyncServerPort());
					tenantModel.setTenantId(tenantEntity.getTntId());
				}
			} else {
				log.info("Saving new device in ACM. No tenant associated!");
				MobileDeviceEntity newDevice = new MobileDeviceEntity();
				newDevice.setImei(mobileDeviceModel.getImei());
				newDevice.setImsi(mobileDeviceModel.getImsi());
				mobileDeviceRepository.save(newDevice);
			}
		}
		return tenantModel;
	}

	@GET
	@Path("/exportTenantsForDeployments")
	@Produces("application/json")
	public ExportTenantsModel displayTenantsForDeployments(){
		ExportTenantsModel tenants = new ExportTenantsModel();
		TenantModel tenant1 = new TenantModel();
		tenant1.setBackofficeIp("192.168.1.1");
		tenant1.setSyncServerIp("192.168.1.2");
		TenantModel tenant2 = new TenantModel();
		tenant2.setBackofficeIp("192.168.2.1");
		tenant2.setSyncServerIp("192.168.2.2");
		ArrayList<TenantModel> tenatsArray = new ArrayList<>();
		tenatsArray.add(tenant1);
		tenatsArray.add(tenant2);
		tenants.setTenants(tenatsArray); 
		return tenants;
	}
}
