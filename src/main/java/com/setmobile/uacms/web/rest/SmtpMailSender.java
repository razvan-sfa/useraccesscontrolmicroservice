/*package com.setmobile.uacms.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.mail.MessagingException;

@Component
public class SmtpMailSender {
	
	
	private JavaMailSender javaMailSender;
	
	@POST
	@Path("/sendMailConfirmation")
	@Consumes("application/json")
	public void send(String to, String subject, String body) throws MessagingException{
		System.out.println("Send mail method");
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		
		helper = new MimeMessageHelper(message, true);
		helper.setSubject(subject);
		helper.setTo(to);
		helper.setText(body, true);
		
		javaMailSender.send(message);
	}
}*/