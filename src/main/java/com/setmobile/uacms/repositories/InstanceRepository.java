package com.setmobile.uacms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.setmobile.uacms.entities.InstanceEntity;

public interface InstanceRepository extends JpaRepository<InstanceEntity, Integer>{

}
