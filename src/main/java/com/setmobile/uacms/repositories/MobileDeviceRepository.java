package com.setmobile.uacms.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.setmobile.uacms.entities.MobileDeviceEntity;

public interface MobileDeviceRepository extends
		JpaRepository<MobileDeviceEntity, Integer> {

	MobileDeviceEntity findByImei(String imei);

	MobileDeviceEntity findByImsi(String imsi);

	MobileDeviceEntity findByUser(String user);
	
	List<MobileDeviceEntity> findByImeiOrImsiOrUser(String imei, String imsi,
			String user);

}
