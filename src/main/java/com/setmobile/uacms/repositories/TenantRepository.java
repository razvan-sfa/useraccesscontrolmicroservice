package com.setmobile.uacms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.setmobile.uacms.entities.TenantEntity;

public interface TenantRepository extends JpaRepository<TenantEntity, Integer> {
	
	TenantEntity findByVerificationToken(String verificationToken);	
	TenantEntity findByEmail(String email);
}
