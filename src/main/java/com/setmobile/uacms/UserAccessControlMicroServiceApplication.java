package com.setmobile.uacms;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.spring.JaxRsConfig;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

import com.setmobile.uacms.web.rest.UserAccessControlWebServices;

@SpringBootApplication
@EnableAsync
@Import({ JaxRsConfig.class })
public class UserAccessControlMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserAccessControlMicroServiceApplication.class, args);
	}

	@Bean
	public ServletRegistrationBean servletRegistrationBean(ApplicationContext context) {
		return new ServletRegistrationBean(new CXFServlet(), "/services/*");
	}

	@Bean
	@Autowired
	public Server rsServer(UserAccessControlWebServices userAccessControlWebServices) {
		JAXRSServerFactoryBean endpoint = new JAXRSServerFactoryBean();
		endpoint.setServiceBean(userAccessControlWebServices);
		endpoint.setAddress("/uac");
		JacksonJsonProvider provider = new JacksonJsonProvider();
		List<Object> providers = new ArrayList<Object>();
		providers.add(provider);
		endpoint.setProviders(providers);
		return endpoint.create();
	}
}
