package com.setmobile.uacms.model;

import java.util.ArrayList;

public class ExportTenantsModel {
	ArrayList<TenantModel> tenants;

	public ArrayList<TenantModel> getTenants() {
		return tenants;
	}

	public void setTenants(ArrayList<TenantModel> tenants) {
		this.tenants = tenants;
	}

	@Override
	public String toString() {
		return "ExportTenantsModel [tenants=" + tenants + "]";
	}

}
/*
class Tenant {
	private String ipBackoffice;
	private String ipDatabase;

	public String getIpBackoffice() {
		return ipBackoffice;
	}

	public void setIpBackoffice(String ipBackoffice) {
		this.ipBackoffice = ipBackoffice;
	}

	public String getIpDatabase() {
		return ipDatabase;
	}

	public void setIpDatabase(String ipDatabase) {
		this.ipDatabase = ipDatabase;
	}

	@Override
	public String toString() {
		return "Tenant [ipBackoffice=" + ipBackoffice + ", ipDatabase=" + ipDatabase + "]";
	}

}
*/