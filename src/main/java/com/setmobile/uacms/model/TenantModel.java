package com.setmobile.uacms.model;

import javax.persistence.Column;

public class TenantModel {

	private String backofficeIp;
	private Integer backofficePort;
	private String syncServerIp;
	private Integer syncServerPort;
	private Integer tenantId;

	public TenantModel() {
	}

	public String getBackofficeIp() {
		return backofficeIp;
	}

	public void setBackofficeIp(String backofficeIp) {
		this.backofficeIp = backofficeIp;
	}

	public Integer getBackofficePort() {
		return backofficePort;
	}

	public void setBackofficePort(Integer backofficePort) {
		this.backofficePort = backofficePort;
	}

	public String getSyncServerIp() {
		return syncServerIp;
	}

	public void setSyncServerIp(String syncServerIp) {
		this.syncServerIp = syncServerIp;
	}

	public Integer getSyncServerPort() {
		return syncServerPort;
	}

	public void setSyncServerPort(Integer syncServerPort) {
		this.syncServerPort = syncServerPort;
	}

	public Integer getTenantId() {
		return tenantId;
	}

	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public String toString() {
		return "TenantModel [backofficeIp=" + backofficeIp + ", backofficePort=" + backofficePort + ", syncServerIp="
				+ syncServerIp + ", syncServerPort=" + syncServerPort + ", tenantId=" + tenantId + "]";
	}

}
