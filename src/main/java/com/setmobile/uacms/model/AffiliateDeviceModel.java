package com.setmobile.uacms.model;

public class AffiliateDeviceModel {
	private Integer tenant;
	private String imei;
	private String imsi;
	
	public AffiliateDeviceModel() {
	}

	public Integer getTenant() {
		return tenant;
	}

	public void setTenant(Integer tenant) {
		this.tenant = tenant;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	@Override
	public String toString() {
		return "AffiliateDeviceModel [tenant=" + tenant + ", imei=" + imei + ", imsi=" + imsi + "]";
	}
	
	
}
