package com.setmobile.uacms.model;

public class AffiliateRequestModel {
	private String email;
	private String imei;
	private String imsi;
	private String name;
	private String password;

	public AffiliateRequestModel() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "AffiliateRequestModel [email=" + email + ", imei=" + imei + ", imsi=" + imsi + ", name=" + name
				+ ", password=" + password + "]";
	}

}
