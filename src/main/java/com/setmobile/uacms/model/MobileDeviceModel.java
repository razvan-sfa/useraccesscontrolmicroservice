	package com.setmobile.uacms.model;
	
	public class MobileDeviceModel {
	
		private String imei;
		private String imsi;
		private String msisdn;
		private String user;
		private String pass;
		private Integer tntId;
		private Boolean tntOwner;
	
		public Boolean getTntOwner() {
			return tntOwner;
		}
	
		public void setTntOwner(Boolean tntOwner) {
			this.tntOwner = tntOwner;
		}
	
		public String getImei() {
			return imei;
		}
	
		public void setImei(String imei) {
			this.imei = imei;
		}
	
		public String getImsi() {
			return imsi;
		}
	
		public void setImsi(String imsi) {
			this.imsi = imsi;
		}
	
		public String getMsisdn() {
			return msisdn;
		}
	
		public void setMsisdn(String msisdn) {
			this.msisdn = msisdn;
		}
	
		public String getUser() {
			return user;
		}
	
		public void setUser(String user) {
			this.user = user;
		}
	
		public String getPass() {
			return pass;
		}
	
		public void setPass(String pass) {
			this.pass = pass;
		}
	
		public Integer getTntId() {
			return tntId;
		}
	
		public void setTntId(Integer tntId) {
			this.tntId = tntId;
		}
	
		@Override
		public String toString() {
			return "MobileDeviceModel [imei=" + imei + ", imsi=" + imsi + ", msisdn=" + msisdn + ", user=" + user
					+ ", pass=" + pass + ", tntId=" + tntId + ", tntOwner=" + tntOwner + "]";
		}
	
	}
