package com.setmobile.uacms.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "instance")
public class InstanceEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ins_id")
	private Integer insId;
	@Column(name = "provider")
	private String provider;
	@Column(name = "instance_id")
	private String instanceId;
	@Column(name = "instance_name")
	private String instanceName;
	@Column(name = "instance_type")
	private String instanceType;
	@Column(name = "ami_id")
	private String amiId;
	@Column(name = "availability_zone")
	private String availabilityZone;
	@Column(name = "instance_state")
	private String instanceState;
	@Column(name = "vCPUs")
	private String vCPUs;
	@Column(name = "memory")
	private String memory;
	@Column(name = "storage")
	private String storage;
	@Column(name = "key_name")
	private String keyName;
	@Column(name = "security_group")
	private String securityGroup;
	@Column(name = "region")
	private String region;
	@Column(name = "network")
	private String network;
	@Column(name = "dns")
	private String dns;
	@Column(name = "public_ip")
	private String publicIp;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "instanceId")
	List<TenantEntity> tenantEntities;

	public InstanceEntity() {
	}

	public InstanceEntity(Integer insId) {
		this.insId = insId;
	}

	public Integer getInsId() {
		return insId;
	}

	public void setInsId(Integer insId) {
		this.insId = insId;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public String getAmiId() {
		return amiId;
	}

	public void setAmiId(String amiId) {
		this.amiId = amiId;
	}

	public String getAvailabilityZone() {
		return availabilityZone;
	}

	public void setAvailabilityZone(String availabilityZone) {
		this.availabilityZone = availabilityZone;
	}

	public String getInstanceState() {
		return instanceState;
	}

	public void setInstanceState(String instanceState) {
		this.instanceState = instanceState;
	}

	public String getvCPUs() {
		return vCPUs;
	}

	public void setvCPUs(String vCPUs) {
		this.vCPUs = vCPUs;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getSecurityGroup() {
		return securityGroup;
	}

	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getDns() {
		return dns;
	}

	public void setDns(String dns) {
		this.dns = dns;
	}

	public String getPublicIp() {
		return publicIp;
	}

	public void setPublicIp(String publicIp) {
		this.publicIp = publicIp;
	}

	public List<TenantEntity> getTenantEntities() {
		return tenantEntities;
	}

	public void setTenantEntities(List<TenantEntity> tenantEntities) {
		this.tenantEntities = tenantEntities;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (insId != null ? insId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof InstanceEntity)) {
			return false;
		}
		InstanceEntity other = (InstanceEntity) object;
		if ((this.insId == null && other.insId != null) || (this.insId != null && !this.insId.equals(other.insId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "InstanceEntity[ insId=" + insId + " ]";
	}

}
