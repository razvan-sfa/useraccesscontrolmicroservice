package com.setmobile.uacms.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "mobile_device")
public class MobileDeviceEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mdv_id")
	private Integer mdvId;
	@Column(name = "imei")
	private String imei;
	@Column(name = "imsi")
	private String imsi;
	@Column(name = "msisdn")
	private String msisdn;
	@Column(name = "user")
	private String user;
	@Column(name = "pass")
	private String pass;
	@Column(name = "tnt_owner")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean tntOwner;

	@ManyToOne
	@JoinColumn(name = "tnt_id")
	private TenantEntity tenant;

	public MobileDeviceEntity() {
	}

	public MobileDeviceEntity(Integer mdvId) {
		this.mdvId = mdvId;
	}

	public Integer getMdvId() {
		return mdvId;
	}

	public void setMdvId(Integer mdvId) {
		this.mdvId = mdvId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public TenantEntity getTenant() {
		return tenant;
	}

	public void setTenant(TenantEntity tenant) {
		this.tenant = tenant;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Boolean getTntOwner() {
		return tntOwner;
	}

	public void setTntOwner(Boolean tntOwner) {
		this.tntOwner = tntOwner;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (mdvId != null ? mdvId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof MobileDeviceEntity)) {
			return false;
		}
		MobileDeviceEntity other = (MobileDeviceEntity) object;
		if ((this.mdvId == null && other.mdvId != null) || (this.mdvId != null && !this.mdvId.equals(other.mdvId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "entities.MobileDeviceEntity[ mdvId=" + mdvId + " ]";
	}

}
