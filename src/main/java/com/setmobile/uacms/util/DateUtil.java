package com.setmobile.uacms.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

	public static String getCurrentTimestamp() {
		Date dt = new java.util.Date();
		java.text.SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(dt);
		return currentTime;
	}
}
