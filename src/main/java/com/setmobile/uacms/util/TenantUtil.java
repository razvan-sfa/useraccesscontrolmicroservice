package com.setmobile.uacms.util;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.setmobile.uacms.entities.TenantEntity;
import com.setmobile.uacms.repositories.TenantRepository;

@Component
public class TenantUtil {

	TenantRepository tenantRepository;

	@Autowired
	public void TenantUtil(TenantRepository tenantRepository) {
		this.tenantRepository = tenantRepository;
	}

	public final String getNextCustomerIdentificationNumber(TenantEntity tenantEntity) {
		Boolean uniqueCIN = false;
		Random rnd = new Random();
		String cin = "";

		while (!uniqueCIN) {
			cin = "AVS";
			cin = cin + new Integer((int) (1000 + rnd.nextInt(9000))).toString();
			uniqueCIN = true;
			List<TenantEntity> tenants = tenantRepository.findAll();
			for (TenantEntity tenant : tenants) {

				if ((!tenant.equals(tenantEntity)) && (tenant.getCIN() != null) && (tenant.getCIN() != "")
						&& (tenant.getCIN().equals(cin)))
					uniqueCIN = false;
			}
		}
		return cin;
	}
}
