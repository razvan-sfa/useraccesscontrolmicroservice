package com.setmobile.uacms.util;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.apache.log4j.Logger;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.setmobile.uacms.entities.InstanceEntity;
import com.setmobile.uacms.entities.MobileDeviceEntity;
import com.setmobile.uacms.entities.TenantEntity;
import com.setmobile.uacms.repositories.InstanceRepository;
import com.setmobile.uacms.repositories.MobileDeviceRepository;
import com.setmobile.uacms.repositories.TenantRepository;

@Component
public class InstanceLookupService {

	@Value("${amazon.webservices.url}")
	String AMAZON_WEBSERVICES_INSTANCE_GENERATOR_URL;

	@Autowired
	private JavaMailSender javaMailSender;
	//this bean is created automatically by Spring Boot, if spring.mail.host exists in application.properties

	@Autowired
	private BackofficeConnection backofficeConnection;
	
	Logger log = Logger.getLogger(InstanceLookupService.class);

	InstanceRepository instanceRepository;
	TenantRepository tenantRepository;
	MobileDeviceRepository mobileDeviceRepository;
	
	@Autowired
	public InstanceLookupService(InstanceRepository instanceRepository, TenantRepository tenantRepository, MobileDeviceRepository mobileDeviceRepository) {
		super();
		this.instanceRepository = instanceRepository;
		this.tenantRepository = tenantRepository;
		this.mobileDeviceRepository = mobileDeviceRepository;
	}

	@Async
	public void associateTenantToInstace(TenantEntity tenantEntity) throws InterruptedException {
		InstanceEntity instance = null;
		Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
		WebTarget target = client.target(AMAZON_WEBSERVICES_INSTANCE_GENERATOR_URL).path("/getAvailableInstance");
		Integer instanceId = target.request().get(Integer.class);
		if (instanceId != null) {
			log.info("Found instance with id: " + instanceId);
			instance = instanceRepository.findOne(instanceId);
			log.info("Found instance: " + instance.getPublicIp());
		} else {
			log.info("No available instance found! Creating a new instace...");
			target = client.target(AMAZON_WEBSERVICES_INSTANCE_GENERATOR_URL).path("/create");
			instance = target.request().get(InstanceEntity.class);
			log.info("New instance created: " + instance.toString());
		}

		tenantEntity.setBackofficeIp(instance.getPublicIp());
		tenantEntity.setSyncServerIp(instance.getPublicIp());
		tenantEntity.setBackofficePort(8080);
		tenantEntity.setSyncServerPort(8084);
		tenantEntity.setInstanceId(instance);
		tenantEntity = tenantRepository.save(tenantEntity);

		log.info("Wait for Tomcat processes on the new created instance to start");
		backofficeConnection.waitForServerPort(tenantEntity.getBackofficeIp(), tenantEntity.getBackofficePort());
		
		backofficeConnection.initializeTenant(tenantEntity.getTntId());
		backofficeConnection.addTenantFolderStructure(tenantEntity);
		
		log.info("Find if tenant with id:" + tenantEntity.getTntId() + " has an mobile device in database.");
		List<MobileDeviceEntity> devices = mobileDeviceRepository.findAll();
		//after SignUp one tenant could have at most one mobile devices in the database 
		//if it has, add the device to backoffice and we create an agent in backoffice
		if (!devices.isEmpty() && devices.size() == 1){
			backofficeConnection.addDevice(tenantEntity, devices.get(0));
			backofficeConnection.addAgent(tenantEntity, null, null, tenantEntity.getContactPerson(), tenantEntity.getEmail(), tenantEntity.getPhone(), devices.get(0).getImei());
		}
		
		List<MobileDeviceEntity> mobileList = tenantEntity.getMobileDeviceEntities();
		
		if (!mobileList.isEmpty()){
			log.info("This tenant has made the SignUp from amobile device. We add his device to backoffice.");
			
		}
		
		
		// send confirmation e-mail
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setSubject("Avansales Cloud account ready");
			helper.setTo(tenantEntity.getEmail());
			String body = "<html> <body>" + "<h2>Your Avansales Cloud System is up and ready to go!</h2>"
					+ "<p>To access your own Avansales Cloud click the link below:</p> <a href='http://backofficeIp:backofficePort/backoffice/'>http://backofficeIp:backofficePort/backoffice/</a>"
					+ "<br> <br> Kind regards!<br> Avansales Cloud Team<br>" + "</body> </html>";
			body = body.replaceAll("backofficeIp", tenantEntity.getBackofficeIp());
			body = body.replaceAll("backofficePort", tenantEntity.getBackofficePort().toString());
			helper.setText(body, true);
			log.info("Mail body: " + body.toString());
			javaMailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}
