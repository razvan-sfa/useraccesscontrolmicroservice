package com.setmobile.uacms.util;

import java.net.Socket;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.setmobile.uacms.entities.MobileDeviceEntity;
import com.setmobile.uacms.entities.TenantEntity;
import com.setmobile.uacms.repositories.InstanceRepository;
import com.setmobile.uacms.repositories.TenantRepository;

@Component
public class BackofficeConnection {

	private static Logger log = Logger.getLogger(BackofficeConnection.class);

	@Value("${backoffice.dabatase.name}")
	private String backofficeDatabaseName;

	@Value("${backoffice.dabatase.port}")
	private Integer backofficeDatabasePort;

	@Value("${backoffice.dabatase.user}")
	private String backofficeDatabaseUser;

	@Value("${backoffice.dabatase.pass}")
	private String backofficeDatabasePass;

	private static Connection connection = null;
	private static PreparedStatement ps1 = null;
	private CallableStatement cs1 = null;

	@Autowired
	TenantRepository tenantRepository;

	@Autowired
	InstanceRepository instanceRepository;

	@Autowired
	public BackofficeConnection(TenantRepository tenantRepository, InstanceRepository instanceRepository) {
		this.tenantRepository = tenantRepository;
		this.instanceRepository = instanceRepository;
	}

	// public Boolean initializeTenant(String backofficeIp, TenantEntity tenant)
	// {
	public Boolean initializeTenant(Integer idTenant) {
		log.info("InitializeTenant with idTenant: " + idTenant.toString());
		TenantEntity tenant = tenantRepository.findOne(idTenant);
		waitForServerPort(tenant.getBackofficeIp(), 3306);

		try {
			connection = DriverManager.getConnection("jdbc:mysql://" + tenant.getBackofficeIp() + ":"
					+ backofficeDatabasePort + "/" + backofficeDatabaseName, backofficeDatabaseUser,
					backofficeDatabasePass);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String addTenant = "{call addTenant(?,?,?,?,?)}";
		try {
			cs1 = connection.prepareCall(addTenant);
			cs1.setInt(1, tenant.getTntId()); // id
			cs1.setString(2, tenant.getName()); // name
			cs1.setString(3, tenant.getEmail()); // email
			cs1.setString(4, tenant.getCIN()); // CIN CustomerIdentification
												// Number
			cs1.setString(5, "pass"); // pass
			ResultSet rs1 = cs1.executeQuery();
			log.info(rs1.toString());
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean waitForServerPort(String serverIP, int serverPort) {
		int maxRetries = 10;
		int retry = 0;
		Boolean isUp = false;
		Socket s = null;
		log.info("Wait for port:" + serverPort + " on server:" + serverIP + " to start.");
		while ((!isUp) && (retry < maxRetries)) {
			try {
				s = new Socket(serverIP, serverPort);
				isUp = true;
			} catch (Exception e) {
				try {
					log.info("Retry " + retry);
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			} finally {
				if (s != null)
					try {
						s.close();
					} catch (Exception e) {
					}
			}
			retry++;
		}
		return isUp;
	}
	
	public void addTenantFolderStructure(TenantEntity tenantEntity){
		log.info("Creating folder structure for tenant id: " + tenantEntity.getTntId() + ", with CIN " + tenantEntity.getCIN());
		Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
		Response response;
		WebTarget target;
		Form objForm;
		
		target = client.target("http://" + tenantEntity.getBackofficeIp() + ":" + tenantEntity.getBackofficePort()).path("backoffice/servlet/InitializeTenantServlet");
		objForm = new Form();
		objForm.param("ws_action", "createFolders");
		objForm.param("tenant", tenantEntity.getTntId().toString());
		response = target.request().post(Entity.form(objForm));
		log.info("Response code after InitializeTenantServlet: " + response.getStatus());
		log.info("Response message " + response.readEntity(String.class));
	}
	
	public void addDevice(TenantEntity tenantEntity, MobileDeviceEntity mobileDeviceEntity){
		log.info("Adding device with IMEI: " + mobileDeviceEntity.getImei() + ", to tenantId " + tenantEntity.getTntId() + ", on backoffice at IP: " + tenantEntity.getBackofficeIp());
		Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
		Response response;
		WebTarget target;
		Form objForm;
		
		target = client.target("http://" + tenantEntity.getBackofficeIp() + ":" + tenantEntity.getBackofficePort()).path("backoffice/servlet/SysDeviceServlet");
		objForm = new Form();
		objForm.param("ws_action", "addDevice");	
		objForm.param("tenant", tenantEntity.getTntId().toString());
		objForm.param("imei", mobileDeviceEntity.getImei());
		objForm.param("imsi", mobileDeviceEntity.getImsi());
		response = target.request().post(Entity.form(objForm));
		log.info("Response code after SysDeviceServlet: " + response.getStatus());
		log.info("Response message " + response.readEntity(String.class));
		
	}
	
	public void addAgent(TenantEntity tenantEntity, String ag_user, String ag_password, String ag_nume, String ag_email, String ag_phone, String imei){
		log.info("Adding new agent on backoffice");
		Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
		Response response;
		WebTarget target;
		Form objForm;
		
		target = client.target("http://" + tenantEntity.getBackofficeIp() + ":" + tenantEntity.getBackofficePort()).path("backoffice/servlet/AgentUpdateServlet");
		objForm = new Form();
		objForm.param("action", "addAgent");	
		objForm.param("tenant", tenantEntity.getTntId().toString());
		objForm.param("ag_user", ag_user);
		objForm.param("ag_password", ag_password);
		objForm.param("ag_nume", ag_nume);
		objForm.param("ag_email", ag_email);
		objForm.param("ag_phone", ag_phone);
		objForm.param("imei", imei);
		response = target.request().post(Entity.form(objForm));
		log.info("Response code after SysDeviceServlet: " + response.getStatus());
	}
}

